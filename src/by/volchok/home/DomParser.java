package by.volchok.home;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DomParser {

    public void parseDocument(String file) {
        File xmlFile = new File(file);//создаём файл
        Document domXmlFile = null;//создаём документ,который хранит DOM
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();//подготовка
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            domXmlFile = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException e) {
            System.out.println("Парсер неверно сконфигурирован");
            e.printStackTrace();
        } catch (SAXException e) {
            System.out.println("Файл не может быть парсирован");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Если передан не XML-файл,то возникает ошибка ввода-вывода");
            e.printStackTrace();
        }
        Element root = domXmlFile.getDocumentElement();//получаем корневой элемент
        System.out.println("Корневой элемент " + root.getNodeName());
        NodeList children = root.getChildNodes();//получаем детей
        childReader(root);


    }
    private void childReader(Node root){
        System.out.println(root.getNodeName() + " = " + root.getNodeValue());//выводим имя нода и значение нода
        for (Node child = root.getFirstChild();child != null; child = child.getNextSibling()) {
            childReader(child);
        }

    }
}
